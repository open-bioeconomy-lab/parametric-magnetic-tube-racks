# Parametric Magnetic Tube Racks

This repository hosts OpenSCAD files for parametric magnetic tube racks based on the design for BOMB microtube rack by Tomasz Jurkowsk 

([BOMB.bio Protocols](https://bomb.bio/protocols/) | [Thingiverse](https://www.thingiverse.com/thing:3199242))


Copyright 2020  - Open Bioeconomy Lab //
Software released under MIT License //
Hardware released under CERN Open Hardware License v1.2

## To use the design file

1.  Download OpenSCAD https://www.openscad.org/ and review a [basic OpenSCAD tutorial](https://all3dp.com/2/openscad-tutorial-for-beginners-5-easy-steps/) so you know where the **Render** and **Export as STL** buttons are.
2.  Open the relevant file in the [OpenSCAD folder](https://gitlab.com/open-bioeconomy-lab/parametric-magnetic-tube-racks/-/tree/master/OpenSCAD)
3.  Adjust the numerical values at the top of the file to change the number of tubes you want to use and the tube and [magnet](https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/parametric-magnetic-tube-racks/-/tree/master/Magnets) dimensions. 
4.  Click **Render** and then **Export as STL** ready for importing to your 3D printer slicing software
5.  Insert the magnets. They may be a very snug fit and not require glue but the plastic may expand over time or with changes in temperature so ideally hold in place with a strong glue like an epoxy resin.

## Making things better

I'm an OpenSCAD beginner so if you spot something that could be done better or more elegantly, please [raise an issue](https://gitlab.com/open-bioeconomy-lab/parametric-magnetic-tube-racks/issues) or directly submit a pull request.

## Examples

### Single and double-sided tube rack 

![1 tube render](./Images/single-tube.png)

![1 tube photo 1](./Images/single-tube-img2.png)
![2 tube photo 1](./Images/double-tube-img1.png)

[![Youtube video thumbnail](./Images/youtube-rack-video.png)](https://www.youtube.com/watch?v=-Qi5GlFoTu0)


### Four-tube rack

![4 tubes](https://gitlab.com/open-bioeconomy-lab/parametric-magnetic-tube-racks/-/raw/0966a6974d2e26c4605b394f7bb54e7204cdcb88/Images/4-tube-rack.png)

### Double-sided, eight-tube rack

![double 8 tubes](https://gitlab.com/open-bioeconomy-lab/parametric-magnetic-tube-racks/-/raw/master/Images/double-sided.png)


